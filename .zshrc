# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="fino-time"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git rvm)

source $ZSH/oh-my-zsh.sh

# Customize to your needs...
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/emkay/bin:/usr/local/mysql/bin:/Users/emkay/pear/bin

# Aliases
alias gis='git status'
alias ll='ls -l'
alias lla='ls -la'
alias dr='drush'
alias ping='ping -c 10'
alias vst='vagrant status'
alias vup='vagrant up'
alias vdn='vagrant halt'
alias dkr='docker'

if [ $UID -ne 0 ]; then
    alias reboot='sudo reboot'
    alias update='sudo apt-get update'
    alias upgrade='sudo apt-get upgrade'
fi

### Added by the Heroku Toolbelt
export PATH="/usr/local/heroku/bin:$PATH"

export PERL_LOCAL_LIB_ROOT="/Users/emkay/perl5:$PERL_LOCAL_LIB_ROOT";
export PERL_MB_OPT="--install_base "/Users/emkay/perl5"";
export PERL_MM_OPT="INSTALL_BASE=/Users/emkay/perl5";
export PERL5LIB="/Users/emkay/perl5/lib/perl5:$PERL5LIB";
export PATH="/Users/emkay/perl5/bin:$PATH";

PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting

export DOCKER_HOST=tcp://:2375
