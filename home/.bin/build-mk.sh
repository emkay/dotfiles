#!/bin/bash

# Auth once for all installs
sudo -v

# Install oh-my-zsh
curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh

# Install homebrew
ruby -e "$(curl -fsSL https://raw.github.com/mxcl/homebrew/go/install)"

# Install homebrew applications
brew install tig
brew install drush
brew install httrack

#Install homebrew-cask
brew tap phinze/homebrew-cask
brew install brew-cask

# Install cask applications
brew cask install alfred
brew cask install android-file-transfer
brew cask install appcleaner
brew cask install asepsis
brew cask install bartender
brew cask install bettertouchtool
brew cask install carbon-copy-cloner
brew cask install dropbox
brew cask install evernote
brew cask install expandrive
brew cask install fantastical
brew cask install filezilla
brew cask install firefox
brew cask install forklift
brew cask install gfxcardstatus
brew cask install google-chrome
brew cask install google-hangouts
brew cask install handbrake
brew cask install integrity
brew cask install kindle
brew cask install libreoffice
brew cask install miro
brew cask install onepassword
brew cask install opera
brew cask install phpstorm
brew cask install rubymine
brew cask install sequel-pro
brew cask install skitch
brew cask install skype
brew cask install sonos
brew cask install spotify
# !! brew cask install sugarsync
brew cask install textwrangler
brew cask install totalterminal
brew cask install vagrant
brew cask install virtualbox
brew cask install vlc
brew cask install silverlight
brew cask install steam

#Gems & Stuff

# Install RVM
\curl -sSL https://get.rvm.io | bash -s stable --ruby

# Create default gemset
rvm gemset create default

# Compass and plugins
gem install compass
gem install sass-globbing

# Standard gems
gem install bundler
gem install homesick
gem install terminal-notifier
# gem install bundle-update

# Install Wraith dependencies
brew install imagemagick
brew install phantomjs

# Install wraith
cd ~/
git clone https://github.com/BBC-News/wraith wraith
cd ~/wraith
bundle install

# Install jekyll
gem install jekyll

# Install some dependencies for emkay.io website
gem install kramdown

# Install pagoda
gem install pagoda-jekyll

# Tidy up the mess
brew cleanup
gem cleanup
