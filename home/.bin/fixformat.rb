#!/usr/bin/env ruby -w

# Whip the FreeAgent output into some sort of shape which can be imported into Excel without losing info
# Export must be for a specific project rather than for all client projects.

# Take file path from ARGS
in_file = ARGV[0]

if in_file == nil
  puts 'Usage: ./fixformat.rb [path/to/input.file]'
  exit
end

# Check file exists
if File.exist?(in_file)

  file_str = IO.read(in_file)

  File.open(in_file+'.fixed.csv', "w") do |new_file|
    # Replace double-quotes
    tmp_str = file_str.gsub!('"', "'")
    # Re-write the file
    new_file.puts tmp_str.gsub!(/([0-9]{2}) (\w{3}) ([0-9]{2})\t([^\t]*)\t(.*)\t (.*)\n(.*)\n([0-9]{1,2}:[0-9]{2})?/i, '"\1","\2","20\3","\4","\5","\6","\7","\8"')
  end

  puts "File fixed, written to: #{in_file}.fixed.csv"

else
  puts "#{in_file} does not exist"
  exit
end