brews = %w[
  tig
  drush
  httrack
  git-ftp
  node
]

casks = %w[
  airdisplay
  alfred
  android-file-transfer
  appcleaner
  asepsis
  bartender
  base
  bettertouchtool
  carbon-copy-cloner
  copy
  dropbox
  evernote
  expandrive
  fantastical
  filezilla
  firefox
  forklift
  gfxcardstatus
  google-chrome
  google-hangouts
  google-refine
  handbrake
  integrity
  jawbone-updater
  kindle
  libreoffice
  logitech-myharmony
  miro
  onepassword
  opera
  pgadmin3
  phpstorm
  rubymine
  sequel-pro
  skitch
  skype
  sonos
  sophos-anti-virus-home-edition
  spotify
  textwrangler
  torbrowser
  totalterminal
  vagrant
  virtualbox
  vlc
  silverlight
  steam
]

gems = %w[
  compass
  sass-globbing
  bundler
  homesick
  terminal-notifier
  methadone
  bundler
  homesick
  methadone
  imagemagick
  phantomjs
  jekyll
  kramdown
  pagoda-jekyll
]

nodes = %w[
  bower
]