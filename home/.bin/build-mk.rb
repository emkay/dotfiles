#!/usr/bin/env ruby -w

# import list of applications
require 'apps.rb'

# Auth once for all installs
# sudo -v

# Install oh-my-zsh
return `curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh`

# Install homebrew
return `ruby -e "$(curl -fsSL https://raw.github.com/mxcl/homebrew/go/install)"`

# Install RVM
return `\curl -sSL https://get.rvm.io | bash -s stable --ruby`

# TODO: Install pear
# curl -O  http://pear.php.net/go-pear.phar

# Install homebrew applications
brews.each {|brew| return `brew install #{brew}` }

# Install nodejs apps
nodes.each {|node| return `npm install -g #{node}` }

#Install homebrew-cask
return `brew tap phinze/homebrew-cask`
return `brew install brew-cask`

# Install cask applications

# Loop through the array installing as we go
# TODO: Investigate 'rescue LocalJumpError' to catch things when an install fails.
# TODO: Add some nice messages into the process
casks.each { |cask| return `brew cask install #{cask}` }

#Gems & Stuff

# Create default gemset
return `rvm gemset create default`

# Install gems
gems.each { |gem| return `gem install #{gem}` }

# Install wraith
return `cd ~/`
return `git clone https://github.com/BBC-News/wraith wraith`
return `cd ~/wraith`
return `bundle install`

# Tidy up the mess
return `brew cleanup`
return `gem cleanup`
