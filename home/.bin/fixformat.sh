#!/bin/zsh

# Whip the FreeAgent output into some sort of shape which can be imported into Excel without losing info
# Export must be for a specific project rather than for all client projects.

if [[ -z "$1" ]]
then
	echo "Usage: $0 [path/to/input.file]"
	exit
else
	echo '===================='
	echo '= Reformatting:'
	echo "= $1"
	echo '===================='
	perl -w -0777 -i.original -pe 's/([0-9]{2}) (\w{3}) ([0-9]{2})\t([^\t]*)\t (.*)\n(.*)\n(.*)/"$1","$2","20$3","$4","$5","$6","$7"/g' $1
fi