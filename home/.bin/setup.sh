#!/bin/bash

cd ~/
ln -s ~/dotfiles/.zshrc ~/.zshrc
echo ====
echo .zshrc file linked
echo ====

ln -s ~/dotfiles/.zlogin ~/.zlogin
echo ====
echo .zlogin file linked
echo ====

ln -s ~/dotfiles/.gitconfig ~/.gitconfig
echo ====
echo .gitconfig file linked
echo ====

ln -s ~/dotfiles/.gitignore_global ~/.gitignore_global
echo ====
echo .gitignore_global file linked
echo ====

# ln -s ~/dotfiles/mk-fino-rimw.zsh-theme ~/.oh-my-zsh/themes/mk-fino-time.zsh-theme
# echo ====
# echo theme linked
# echo ====
